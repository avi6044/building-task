import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuildingListComponent } from './components/building-list/building-list.component';

const routes: Routes = [
  {path:'',redirectTo:'building-list',pathMatch:'full'},
  {
    path:'building-list',
    component:BuildingListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
