export interface BuildingModel {
    id:number;
    name:string;
    building:string ;
    building_tower:string;
    property_type : string;
    configuration_name:string;
    min_price:number;
    bedroom: number;
    bathroom:number;
    half_bathroom:number;
}