import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getHeaderName'
})
export class GetHeaderNamePipe implements PipeTransform {

  transform(colName:string): string {
    if(colName.includes('_')) {
      const newColName = colName.replaceAll('_', ' ')
      return newColName.toUpperCase()
    }
    return colName.toUpperCase();
  }

}
