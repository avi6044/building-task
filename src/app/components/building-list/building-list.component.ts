import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { BuildingModel } from 'src/app/model/building-model';
import { BuildingService } from 'src/app/services/building.service';

@Component({
  selector: 'app-building-list',
  templateUrl: './building-list.component.html',
  styleUrls: ['./building-list.component.scss']
})
export class BuildingListComponent implements OnInit {
  buildingList = new MatTableDataSource<BuildingModel>([])
  displayedColumns: string[] = [];
  columnsToDisplay: string[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  isLoader:boolean = true;

  filterForm = new FormGroup(
     {
      search: new FormControl(''),
      price: new FormControl(''),
      config: new FormControl(''),
     }
   );

  configurationsList:Array<any> = [] 

  constructor(private buildingService:BuildingService,
    private spinner:NgxSpinnerService) {
        this.buildingList.filterPredicate = ((data:BuildingModel, filter:any) => {
          const a = !filter.price || data.min_price > filter.price;
          const b = !filter.search || JSON.stringify(data).toLowerCase().includes(filter.search);
          let isConfigurationAvailable = false;
          if (filter.config.length) {
            for (const d of filter.config) {
              if (data.configuration_name.toString().trim() == d) {
                isConfigurationAvailable = true;
              }
            }
          } else {
            isConfigurationAvailable = true;
          }
          return a && b && isConfigurationAvailable;
        }) as () => boolean;

        this.filterForm.valueChanges.subscribe(value => {
          const filter = {...value, search: value.search.trim().toLowerCase()} as string;
          this.buildingList.filter = filter;
        });
    }

  ngAfterViewInit() {
    this.buildingList.sort = this.sort;
  }
  
  ngOnInit(): void {
    this.getBuildingsData();
  }

  

  /**
   *  getting a lists of building data 
   */
  getBuildingsData() {
    this.spinner.show();
    this.buildingService.getBuildingList().subscribe(response => {
      for (let build in response[0]) {
        if(build != 'id') {
          this.displayedColumns.push(build);
        }
      }
      this.columnsToDisplay =  this.displayedColumns;
      this.setConfiguration(response);
      this.buildingList.data = response;
      this.spinner.hide();
      this.isLoader = false;
      setTimeout(() => {
        this.buildingList.paginator = this.paginator;
        this.buildingList.sort = this.sort
      });
    },(error) => {
      this.spinner.hide();
    })
  }

/**
 * Set unique Array for Configuration
 * @param buildingData 
 */
  setConfiguration(buildingData:any) {
    buildingData.map((item:BuildingModel) => {
       let found  = this.configurationsList.find( conFig => conFig === item.configuration_name )
       if(!found) {
        this.configurationsList.push(item?.configuration_name);
       }
    })
  }

}
